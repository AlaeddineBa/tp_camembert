package model;

import java.awt.Color;
import java.util.Observable;

public class Items extends Observable  implements IItems {
	
	private String name;
	private float value;
	private String description;
	private Color color;
	
	
	public Items(String name, float value, String description, Color color) {
		super();
		this.name = name;
		this.value = value;
		this.description = description;
		this.color = color;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
	}
	
}
