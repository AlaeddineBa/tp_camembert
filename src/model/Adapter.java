package model;
import java.util.ArrayList;

import java.util.Observable;


public class Adapter extends Observable implements IModel {
	
	private Model model;

	public Adapter(String title) {
		super();
		this.model = new Model(title);
	}

	@Override
	public void addItem(Items item) {
		this.model.addItem(item);
		setChanged();
		notifyObservers();
		
	}

	@Override
	public void removeItem(Items item) {
		this.model.removeItem(item);
		setChanged();
		notifyObservers();
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return this.model.getTitle();
	}

	@Override
	public void setTitle(String title) {
		this.model.setTitle(title);
	}

	@Override
	public ArrayList<Items> getItems() {
		return this.model.getItems();
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	@Override
	public void update() {
		setChanged();
		notifyObservers();
		
	}
}
