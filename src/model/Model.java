package model;

import java.util.ArrayList;

public class Model implements IModel {
	
	private String title;
	ArrayList<Items> items;
	

	public Model(String title) {
		super();
		this.title = title;
		this.items = new ArrayList<Items>();
	}

	public Model() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<Items> getItems() {
		return items;
	}

	public void setItems(ArrayList<Items> items) {
		this.items = items;
	}

	@Override
	public void addItem(Items item) {
		this.items.add(item);
		
	}

	@Override
	public void removeItem(Items item) {
		this.items.remove(item);
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
	

	
	

}
