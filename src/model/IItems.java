package model;

public interface IItems {

	public String getName();

	public void setName(String name);

	public float getValue();

	public void setValue(float value);

	public String getDescription();

	public void setDescription(String description);

}
