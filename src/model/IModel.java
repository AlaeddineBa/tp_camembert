package model;
import java.util.ArrayList;

public interface IModel {
	void addItem(Items item);
	void removeItem(Items item);
	String getTitle();
	void setTitle(String title);
	ArrayList<Items> getItems();
	void update();

}
