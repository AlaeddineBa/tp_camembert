import java.awt.Color;
import model.Adapter;

import model.Items;
import view.MainView;

public class Main {

	public static void main(String[] args) {
		//Creation du quatre Items
		Items item1 = new Items("Item1", 200, "Desc1", Color.BLUE);
		Items item4 = new Items("Item4", 600, "Desc4", Color.CYAN);
		Items item5 = new Items("Item2", 400, "Desc4", Color.DARK_GRAY);
		Items item2 = new Items("Item10", 400, "Desc4", Color.gray);
		//Creation d'un Model
		Adapter adapter = new Adapter("Budget");
		//Ajouter les Items au Model
		adapter.addItem(item1);
		adapter.addItem(item4);
		adapter.addItem(item5);
		adapter.addItem(item2);
		
		MainView mainView = new MainView(adapter);
		
	}

}
