package controller;

import model.Adapter;
import model.Items;
import view.View;

public class Controller implements IController {

	private Adapter adapter;
	private View view;
	private static int index;

	public Controller(Adapter adapter, View view) {
		super();
		this.adapter = adapter;
		this.view = view;
	}

	public Controller() {
		super();
	}

	public Adapter getAdapter() {
		return adapter;
	}

	public void setAdapter(Adapter adapter) {
		this.adapter = adapter;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public void click(int x, int y) {
		//Si l'Utilisateur a cliqué sur l'un du deux Flaches
		if (this.view.getArrows().get(0).contains(x, y) || this.view.getArrows().get(1).contains(x, y)) {
			//Agrandir l'Item suivant
			if (this.view.getArrows().get(1).contains(x, y)) {
				index = (++index % adapter.getItems().size());
				this.view.setItemSelected(adapter.getItems().get(index));
			} else {
				//Agrandir l'Item precedent
				index--;
				if (index == -1)
					index = adapter.getItems().size() - 1;
				this.view.setItemSelected(adapter.getItems().get(index));
			}
		} else {
			//Sinon Agrandir le l'Item cliqué
			this.view.setItemSelected(returnItem(x, y));
		}
	}
	
	public void addItem(Items item){
		this.adapter.addItem(item);
	}
	
	public void removeItem(Items item){
		this.adapter.removeItem(item);
	}
	//Retourner l'Item où l'Utilisateur a cliqué sinon retourner null s'il a cliqué dehors des Items
	public Items returnItem(double x, double y) {
		for (int i = 0; i < this.view.getArcs().length; i++) {
			if (this.view.getArcs()[i].contains(x, y)) {
				System.out.println(x + " " + y);
				Controller.index = i;
				return this.adapter.getItems().get(i);
			}
		}
		return null;

	}

}
