package controller;


import model.Adapter;
import model.Items;
import view.View;

public interface IController {
	
	Adapter getAdapter();

	void setAdapter(Adapter adapter);

	View getView();

	void setView(View view);
	
	void addItem(Items item);
	
	void removeItem(Items item);
	
	void click(int x, int y);
	

}
