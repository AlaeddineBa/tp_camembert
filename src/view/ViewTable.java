package view;
import javax.swing.table.AbstractTableModel;

import model.IModel;

public class ViewTable extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String title[] = { "Label", "Valeur", "Description" };
	private IModel adapter;

	// Constructeur
	public ViewTable(IModel adapter) {
		this.adapter = adapter;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		switch (columnIndex) {
		case 0:
			this.adapter.getItems().get(rowIndex).setName((String) aValue);
			break;
		case 1:
			this.adapter.getItems().get(rowIndex).setValue(Float.parseFloat((String) aValue));
			break;
		case 2:
			this.adapter.getItems().get(rowIndex).setDescription((String) aValue);
			break;
		}
		this.adapter.update();
	}

	@Override
	public int getRowCount() {
		return this.adapter.getItems().size();
	}

	public int getColumnCount() {
		return title.length;
	}

	public String getColumnName(int columnIndex) {
		return title[columnIndex];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return this.adapter.getItems().get(rowIndex).getName();
		case 1:
			return this.adapter.getItems().get(rowIndex).getValue();
		case 2:
			return this.adapter.getItems().get(rowIndex).getDescription();
		default:
			return null;
		}
	}

}
