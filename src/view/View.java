package view;

import java.awt.Color;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Arc2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JComponent;
import controller.IController;
import model.Adapter;
import model.Items;

public class View extends JComponent implements MouseListener, IView, Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Graphics2D g2d;
	Adapter adapter;
	IController controller;
	Items itemSelected;
	double total;
	Arc2D arcs[];
	ArrayList<Polygon> arrows;

	public Adapter getAdapter() {
		return adapter;
	}

	public Items getItemSelected() {
		return itemSelected;
	}

	public void setItemSelected(Items itemSelected) {
		this.itemSelected = itemSelected;
	}

	public void setAdapter(Adapter adapter) {
		this.adapter = adapter;
	}

	public IController getController() {
		return controller;
	}

	public void setController(IController controller) {
		this.controller = controller;
	}

	public void setArrows(ArrayList<Polygon> arrows) {
		this.arrows = arrows;
	}

	public View() {
		arrows = new ArrayList<Polygon>();
		addMouseListener(this);
	}

	public Arc2D[] getArcs() {
		return arcs;
	}

	public void setArcs(Arc2D[] arcs) {
		this.arcs = arcs;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g2d = (Graphics2D) g;

		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHints(rh);

		total = 0;
		Adapter adaptor = this.adapter;
		arcs = new Arc2D[adapter.getItems().size()];

		// Calcul du total des valeurs des items pour pouvoir dessiner les arcs
		for (int i = 0; i < adaptor.getItems().size(); i++) {
			total += adaptor.getItems().get(i).getValue();
		}

		double valueArc = 0;

		int x = 122;
		int y = 122;
		int w = 255;
		int h = 250;

		// Parcourir les Items
		for (int i = 0; i < adaptor.getItems().size(); i++) {
			// Calculer l'angle d'ouverture
			int angleStart = (int) (valueArc * 360 / total);
			// Calculer l'angle de fermuture
			int angleExtent = (int) (adaptor.getItems().get(i).getValue() * 360 / total);

			Arc2D.Double arc;
			// Si aucun item est selectioné
			if (itemSelected == null)
				arc = new Arc2D.Double(x, y, w, h, angleStart, angleExtent, Arc2D.PIE);
			// Si un Item est Selectioné
			else if (adaptor.getItems().get(i).getName() == itemSelected.getName())
				arc = new Arc2D.Double(x - w / 8, y - h / 8, 5 * w / 4, 5 * h / 4, angleStart, angleExtent, Arc2D.PIE);
			else
				// Si aucun item est selectioné
				arc = new Arc2D.Double(x, y, w, h, angleStart, angleExtent, Arc2D.PIE);
			// Ajouter la valeur de l'Item pour pouvoir dessiner le prochain arc
			valueArc += adaptor.getItems().get(i).getValue();

			g2d.setColor(adaptor.getItems().get(i).getColor());
			g2d.fill(arc);

			arcs[i] = arc;

			// Dessin du cadre pour ecrire le label, descrition et valeur si un
			// Item est selectioné
			if (itemSelected != null) {
				g2d.setPaint(Color.WHITE);
				g2d.fill(new Rectangle2D.Double(160, 422, 200, 120));
				g2d.setPaint(Color.BLACK);
				g2d.drawString("Label: " + itemSelected.getName(), 165, 444);
				g2d.drawString("Description: " + itemSelected.getDescription(), 165, 465);
				g2d.drawString("Valeur: " + itemSelected.getValue() + " €", 165, 485);
			}

		}

		// Dessin du premier cercles
		Arc2D.Double c1 = new Arc2D.Double(Arc2D.OPEN);
		c1.setArc(166, 166, 160, 160, 90, 360, Arc2D.OPEN);
		g2d.setColor(Color.WHITE);
		g2d.fill(c1);

		// Dessin du deuxieme cercle
		Arc2D.Double c2 = new Arc2D.Double(Arc2D.OPEN);
		c2.setArc(182, 182, 130, 130, 90, 360, Arc2D.OPEN);
		g2d.setColor(Color.lightGray);
		g2d.fill(c2);

		// Ecrire le label et le total des Items dans le cercle
		g2d.setFont(new Font("Arial", Font.BOLD, 14));
		g2d.setColor(Color.WHITE);
		g2d.drawString(this.adapter.getTitle(), 217, 240);
		g2d.drawString(Double.toString(total) + " €", 220, 260);

		// Dessin des Fleches
		Polygon arrow1 = new Polygon();
		Polygon arrow2 = new Polygon();

		arrow1.addPoint(500, 190);
		arrow1.addPoint(480, 220);
		arrow1.addPoint(520, 220);

		arrow2.addPoint(500, 270);
		arrow2.addPoint(480, 240);
		arrow2.addPoint(520, 240);

		this.arrows.add(arrow1);
		this.arrows.add(arrow2);

		for (Polygon p : this.arrows) {
			g2d.setColor(Color.BLUE);
			g2d.fill(p);

		}

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		controller.click(arg0.getX(), arg0.getY());
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Observable o, Object arg) {
		repaint();
	}

	public ArrayList<Polygon> getArrows() {
		// TODO Auto-generated method stub
		return this.arrows;
	}

}