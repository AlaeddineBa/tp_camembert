package view;
import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controller.Controller;
import controller.IController;
import model.Adapter;
import model.Items;

public class MainView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static View view;
	private static JTable table;
	private static ViewTable tables;
	private static int i;

	public MainView(Adapter adapter) {
		//La vue
		view = new View();
		//Le controlleur
		IController controller = new Controller(adapter, view);
		
		//Attacher le model au vue
		view.setAdapter(adapter);
		//Attacher le controlleur au vue
		view.setController(controller);

		JFrame frame = new JFrame();
		JPanel container = new JPanel();

		container.setLayout(new BoxLayout(container, BoxLayout.X_AXIS));

		JPanel panel1 = new JPanel(new BorderLayout());
		JPanel panel2 = new JPanel();

		tables = new ViewTable(adapter);
		table = new JTable(tables);

		table.setModel(tables);
	
		//Mettre le model observé dans la vue
		adapter.addObserver(view);

		panel1.add(view);
		panel2.add(new JScrollPane(table));

		JButton addButton = new JButton("Ajouter");
		addButton.setBounds(160, 6, 100, 20);

		JButton removeButton = new JButton("Supprimer");
		removeButton.setBounds(200, 6, 100, 20);

		//Supprimer un Item ou plusieurs apres leurs selection
		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] selection = table.getSelectedRows();
				for (int i = 0; i < selection.length; i++) {
					controller.removeItem(adapter.getItems().get(selection[i]));
					//Construire le nouveau tableau avec les nouvelles données
					table.setModel(new ViewTable(adapter));
					//Refrechir le Tableau
					tables.fireTableDataChanged();
				}
			}
		});
		//Ajouter un Item en Cliquant sur Ajouter 
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Random random = new Random();
				float x = random.nextFloat();
				float y = random.nextFloat();
				float z = random.nextFloat();
				Color color = new Color(x, y, z);

				controller.addItem(new Items("New "+i++, 20, "New", color));

				table.setModel(new ViewTable(adapter));
				tables.fireTableDataChanged();
			}
		});

		container.add(panel1);
		container.add(panel2);
		container.add(addButton);
		container.add(removeButton);

		frame.add(container);
		frame.setSize(1200, 600);
		frame.setVisible(true);

	}

}
